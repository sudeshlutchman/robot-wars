using System;

namespace RobotWars
{

/*
    An IMovable interface is created so that if the game is expanded to allow more than robots on the field
    (such as hazards) then we can give them the ability to move also.

 */
    interface IMovable
    {
        void Move(char command, int x0, int y0, int x1, int y1);
    }
}
