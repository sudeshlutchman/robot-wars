using System;

namespace RobotWars
{

/*
    The Robot class maintains the state of an individual Robot.
    The Robot class expands on the FieldObject by implementing IMovable
    allowing the Robot to move around the Field.
 */
    class Robot : FieldObject, IMovable
    {

        // Uses the base constructor
        public Robot(int x, int y, char heading) : base(x, y, heading){}

        // IMovable implementation
        // If L or R change the direction through addition/subtraction
        // If M then move the robot. Assumption is that the Robots cannot move out of the grid.
        public void Move(char command, int x0, int y0, int x1, int y1)
        {
            
            command = Char.ToUpper(command);
            
            if(command == 'L')
            {
                direction--;
                if(direction < 0)
                {
                    direction = 3;
                }
            }
            else if (command == 'R')
            {
                direction++;
                if(direction > 3)
                {
                    direction = 0;
                }
            }
            else if (command == 'M')
            {
                // If headed North or South
                if(direction == 0)
                {
                    if(Y != y1) yCoord++;
                }
                else if(direction == 2)
                {
                    if(Y != y0) yCoord--;
                }
                // headed West or East
                else if(direction == 1)
                {
                    if(X != x1) xCoord++;
                }
                else // direction = 3
                {
                    if(X != x0) xCoord--;
                }
            }
            else
            {
                // do nothing as command is not implemented.
            }
        }
    }
}
