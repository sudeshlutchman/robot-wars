using System;

namespace RobotWars
{

/*
    The FieldObject class maintains the state of an individual object on the field.
    This base class is used so that the game can be expanded to have more than just robots on the field.
    Also allows the Robot class to focus on doing robot things only.
 */
    abstract class FieldObject
    {
        protected int xCoord = 0;
        protected int yCoord = 0;

        // direction (heading) is stored as an int for convenience
        protected int direction = 0;
        private char[] cardinalPoints = {'N', 'E', 'S', 'W'};

        // No modifiers for coordinate information is are provided since this should be managed
        // through implementing IMovable
        protected int X
        {
            get => xCoord;
        }

        protected int Y
        {
            get => yCoord;
        }

        protected char Heading
        {
            get => cardinalPoints[direction];
        }

        // Converts the Cardinal point char to an int value
        // Defaults to North
        private int CardinalPointToInt(char cp)
        {
            switch (Char.ToUpper(cp))
            {
                case 'N' :
                    return 0;
                case 'E' :
                    return 1;
                case 'S' :
                    return 2;
                case 'W' :
                    return 3;
            }

            return 0;
        }

        // Only one constructor is provided as the following are all required for an object on the field.
        public FieldObject(int x, int y, char heading)
        {
            xCoord = x;
            yCoord = y;
            direction = CardinalPointToInt(heading);
        }

        // Writes the object's position and directionm to the console.
        public void Display()
        {
            Console.WriteLine($"{X} {Y} {Heading}");
        }
    }
}
