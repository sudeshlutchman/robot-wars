﻿using System;

/*
    Due to the simplicity of the problem I did not implement a barebones solution that simply returns the results.
    Instead I opted to create a solution from the standpoint that the code would later be extended into a more full fledge game,
    as opposed to an entry for an algorithm type competition.

    Assumptions:
        1. Memory complexity is not an issue
        2. A List can hold all the robots being used.
                The list of FieldObjects is implemented but left unused as the question allows for infinite 
                robots on the field.
        3. There is no limit to how many robots can occupy the same coordinates
        4. Robots that try to move out of the grid simply will not move.
        5. The input is provided correctly

 */
namespace RobotWars
{

    class GameController
    {
        private Field field;
        private string acceptedCommands = "LRM";

        // Simple method to manage moving the object around the field based on the commands.
        private void MoveRobot(Robot rob, string commands)
        {

            foreach (char command in commands.ToCharArray())
            {
                // do nothing if the command is not in the accepted Commands list
                if(!acceptedCommands.Contains(Char.ToUpper(command))) continue;

                rob.Move(command, field.X0, field.Y0, field.X1, field.Y1);                
            }            
        }

        public void ReadInput()
        {

            // Read the field dimensions.
            string[] input = Console.ReadLine().Split(' ');
            try
            {
                field = new Field(Convert.ToInt32(input[0]), Convert.ToInt32(input[1]));
            }
            catch (System.FormatException)
            {
                Console.WriteLine("Field coordinates not provided in the expected format.");
                throw;
            }


            Robot r2d2;
            // Read the robot input.
            while(true)
            {

                input = Console.ReadLine().Split(' ');
                
                try
                {
                    r2d2 = new Robot(Convert.ToInt32(input[0]), Convert.ToInt32(input[1]), input[2].ToCharArray()[0]);
                }
                catch (System.FormatException)
                {
                    Console.WriteLine("Robot information not provided in the expected format.");
                    throw;
                }

                // Read the commands in and move the robot accordingly.
                MoveRobot(r2d2, Console.ReadLine());

                // Display the Robot's final orientation.
                r2d2.Display();

            }

        }

        static void Main(string[] args)
        {

            GameController gc = new GameController();

            gc.ReadInput();
        }
    }
}
