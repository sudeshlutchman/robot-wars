# Robot Wars

This project was created as part of a code challenge for OpenTable. The objective of the application is to manipulate robots on a field and keep track of them.

## General Thoughts Going In

Due to the simplicity of the problem I did not implement a barebones solution designed to return the results as quickly as possible while using as little memory as possible.
Instead I opted to create a solution from the idea that the could would later be extended into a robust game.

## Assumptions

* Memory complexity is not a major issue.
    * Otherwise one class would have been used
    * A robot would simply be three ints (x, y and direction)
* ~~A List can hold all the robots being used~~
* There is no limit to how many robots can occupy the same coordinates
* Robots that try to move out of the grid simply will not move
* The input is provided correctly.

## Design

The game consist of three main components:

1. GameController:
    1. This handles the overall flow of the game. It controls the other objects and reads the input
2. Field:
    1. The Field object is responsible for keeping track of the playable area
    2. The Field object contains all objects in play
        1. This was implemented into the class but not used by the GameController. The reason being was to avoid memory overflows. I left the List in simply to illustrate the idea.
3. FieldObject (abstract class):
    1. FieldObjects represent objects on the field and keeps track of their states.
    2. The Robot class inherits from FieldObject. In this project only the Robot class uses the FieldObject base, however, the game can be expanded to introduce new FieldObjects such as
        hazards, walls and enemies.
    3. The Robot class implements an IMovable interface. This is used to process the different commands that a Robot can perform. An interface was used so that future FieldObjects
        can choose unique ways to behave based on input.

## Getting Started

The project was built as a console application on .NET Core. All code is in C#.

### Prerequisites

The .NET Core framework is required to get this up and running.

### Installing

Nothing needs to be installed once you have .NET core already setup.


## Running the project

The simplest way to run the project would be to navigate to the directory through command line (terminal or PowerShell console) and then use the "dotnet run" command.
From here the application will accept input as specified in the task.

The application has to be manually terminated (through closing the terminal or exiting the process) as there is no defined ending criteria.


## Tests

No formal tests were written for this project. Different outcomes were experimented on manually.

## Future work

* Implement tests for all classes
* Introduce new FieldObjects
* Give the field the ability to shrink and expand


## Authors

* **Sudesh Lutchman** 
