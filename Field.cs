using System;
using System.Collections.Generic;

namespace RobotWars
{
/*
    The Field class contains the dimensions of the field as well as the 
    objects that are on the field. 
 */
    class Field
    {

        private int x0 = 0, y0 = 0;
        private int x1, y1;
        private List<FieldObject> objects;

        // public modifiers are not provided for x1 and y1 as they should never be changed
        // once the field is instantiated.
        // Could be modified later to create a game where the field can expand/shrink.

        public int X0
        {
            get => x0;
        }

        public int Y0
        {
            get => y0;
        }
        
        public int X1
        {
            get => x1;
        }

        public int Y1
        {
            get => y1;
        }

        public Field(int x1, int y1)
        {
            this.x1 = x1;
            this.y1 = y1;
            objects = new List<FieldObject>();
        }

        // Add an object to the field.
        public bool AddRobot(FieldObject obj)
        {
            // Possible memory exception if too many objects.
            try
            {
                objects.Add(obj);
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        // Get a given object based on the order in which they were listed. (0 based index)
        // Returns null if not found.
        public FieldObject GetObject(int value)
        {

            if(value >= objects.Count) return null;

            return objects[value];
        }

        // Get the latest FieldObject that was added to the field.
        public FieldObject CurrentRobot()
        {
            return GetObject(objects.Count - 1);
        }

    }
}
